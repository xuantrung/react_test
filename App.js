import React, {Component} from 'react'
import {YellowBox} from 'react-native'
import AppNavigation from './screens/AppNavigation'

console.disableYellowBox = true;
export default class App extends Component{
    render(){
        return(
            <AppNavigation />
        );
    }
}