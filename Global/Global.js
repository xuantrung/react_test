import React from 'react'

export const GLOBAL = {
    HEADER_MAX_HEIGHT: 200,
    HEADER_MIN_HEIGHT: 100,
    SIZE_CART: 35
};