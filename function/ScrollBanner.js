import React, {Component} from 'react'
import {
    View, ScrollView, Dimensions
} from 'react-native'
import styles from '../styles/Style'

const screen = Dimensions.get('window')
export default class HorizontalScrollBanner extends Component{
    render(){
        return(
            <ScrollView
                horizontal={true}
                style={{...this.props.style, backgroundColor: 'blue'}}>
                <View 
                    style={{
                        width: '80%',
                        height: '100%',
                        backgroundColor: 'red'
                    }}>

                </View>
            </ScrollView>
        );
    }
}