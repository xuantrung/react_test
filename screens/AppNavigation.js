import {
    createBottomTabNavigator, createAppContainer, createStackNavigator
} from 'react-navigation'
import React from 'react'
import {
    Image
} from 'react-native'
import Home from './Home'
import Search from './Search'
import Notification from './Notification'
import Category from './Category'
import Individual from './Individual'


const ScreenCategory = createStackNavigator({
    ScreenCategory: {screen: Category}
})
const TabBarNavigation = createBottomTabNavigator(
    {
        Home: {
            screen: Home,
            navigationOptions: {
                tabBarIcon: ({ focused }) => {
                    const image = focused ? require('../assets/home_selected.png') : require('../assets/home.png')
                    return <Image source={image}
                            style= {{width: 20, height: 20}} />
                },
                tabBarLabel: 'Trang chủ'
            }
        },
        Category: {
            screen: ScreenCategory,
            navigationOptions: {
                tabBarIcon: ({ focused }) => {
                    const image = focused ? require('../assets/category_selected.png') : require('../assets/category.png')
                    return <Image source={image}
                            style= {{width: 20, height: 20}} />
                },
                tabBarLabel: 'Danh mục',
            }
        },
        Search: {
            screen: Search,
            navigationOptions: {
                tabBarIcon: ({ focused }) => {
                    const image = focused ? require('../assets/search_selected.png') : require('../assets/search.png')
                    return <Image source={image}
                            style= {{width: 20, height: 20}} />
                },
                tabBarLabel: 'Tìm kiếm'
            }
        },
        Notification: {
            screen: Notification,
            navigationOptions: {
                tabBarIcon: ({ focused }) => {
                    const image = focused ? require('../assets/notification_selected.png') : require('../assets/notification.png')
                    return <Image source={image}
                            style= {{width: 20, height: 20}} />
                },
                tabBarLabel: 'Thông báo'
            }
        },
        Individual: {
            screen: Individual,
            navigationOptions: {
                tabBarIcon: ({ focused }) => {
                    const image = focused ? require('../assets/individual_selected.png') : require('../assets/individual.png')
                    return <Image source={image}
                            style= {{width: 20, height: 20}} />
                },
                tabBarLabel: 'Cá nhân',
                title: 'trung'
            }
        }
    }
)

const AppNavigation = createAppContainer(TabBarNavigation)

export default AppNavigation;
