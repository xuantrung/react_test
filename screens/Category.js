import React, {Component} from 'react'
import {
    View, Text, TouchableHighlight
} from 'react-native'

export default class Category extends Component{

    constructor(props){
        super(props)
        this.props.navigation.setParams({
            nameRouter: 'trung'
        })
    }

    static navigationOptions = ({navigation}) => ({
        title: navigation.getParam('nameRouter')
    })

    render(){
        return(
            <View style={{backgroundColor: 'red', flex: 1}}>
                <TouchableHighlight onPress={() => {this.props.navigation.navigate('Individual')}}>
                    <View style={{width: 20, height: 20, backgroundColor: 'white'}}></View>
                </TouchableHighlight>
            </View>
        );
    }
}