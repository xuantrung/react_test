import React, {Component} from 'react'
import {
    View, Animated, StyleSheet, Text, ScrollView, Dimensions, Image, TextInput
} from 'react-native'
import styles from '../styles/Style'
import {GLOBAL} from '../Global/Global'
import ScrollBanner from '../function/ScrollBanner'

const screen = Dimensions.get('window')
export default class Home extends Component{
    constructor(props){
        super(props)
        this.scrollYAnimatedValue = new Animated.Value(0);
        this.array = [];

    }

    componentWillMount(){
        for (var i = 1; i < 20; i++){
            this.array.push(i)
        }
    }

    render(){
        const HEADER_SCROLL_DISTANCE = ( GLOBAL.HEADER_MAX_HEIGHT - GLOBAL.HEADER_MIN_HEIGHT )
        const imageOpacity = this.scrollYAnimatedValue.interpolate({
            inputRange: [0, HEADER_SCROLL_DISTANCE / 2, HEADER_SCROLL_DISTANCE],
            outputRange: [1, 0.3, 0],
            extrapolate: 'clamp',
        });

        const imageZIndex = this.scrollYAnimatedValue.interpolate({
            inputRange: [0, 45, 45 + 20 ],
            outputRange: [0, 0, 10],
            extrapolate: 'clamp',
        });
        const headerHeight = this.scrollYAnimatedValue.interpolate(
        {
            inputRange: [ 0, HEADER_SCROLL_DISTANCE ],
            outputRange: [ GLOBAL.HEADER_MAX_HEIGHT, GLOBAL.HEADER_MIN_HEIGHT ],
            extrapolate: 'clamp'
        });

        const headerLayout = this.scrollYAnimatedValue.interpolate(
            {
                inputRange: [ 0, 45 ],
                outputRange: [ GLOBAL.HEADER_MAX_HEIGHT, GLOBAL.HEADER_MIN_HEIGHT - 20],
                extrapolate: 'clamp'
            });

        const searchbarWidth = this.scrollYAnimatedValue.interpolate(
            {
                inputRange: [0, 45],
                outputRange: [ 0, GLOBAL.SIZE_CART + 5 ],
                extrapolate: 'clamp'
            });
        
        const searchbarTop = this.scrollYAnimatedValue.interpolate(
        {
            inputRange: [ 0, 45 ],
            outputRange: [ 75, 30 ],
            extrapolate: 'clamp'
        }); 

        const bannerTop = this.scrollYAnimatedValue.interpolate(
            {
                inputRange: [ 0, 45, 150 ],
                outputRange: [ 130, 85, -20 ],
                extrapolate: 'clamp'
            }); 

        return(
            <View style={styles.container}>
                <ScrollView
                    contentContainerStyle={styles.scrollContent}
                    bounces={false}
                    scrollEventThrottle={16}
                        // tần suất gửi các event trong khi scroll tính = ms, 
                        // số thấp thì độ chính xác lớn nhưng lượng thông tin gửi qua trong 1ms nhiều -> animation ít mượt hơn
                        // max = 16 -> animation mượt hơn
                    onScroll={Animated.event(
                        [{nativeEvent: {contentOffset: {y: this.scrollYAnimatedValue}}}]
                    )}>
                    {this._renderNonSense()}
                </ScrollView>

                <Animated.View style={[styles.animatedHeader, {height: headerHeight}]}>
                    <Animated.Image
                        style={[ styles.backgroundImage, {height: headerHeight}]}
                        source={require('../assets/header_image.jpg')}
                    />
                    <Animated.Image
                        style={[ styles.backgroundImage, {height: headerLayout, zIndex: imageZIndex}]}
                        source={require('../assets/header_image.jpg')}
                    />
                    
                    <View style={styles.contentHeader}>
                        <View style={styles.containerLogo}>
                            <Animated.Image 
                                style={[styles.logo, {opacity: imageOpacity}]}
                                source={require('../assets/tiki.png')}/>
                            <Image style={styles.cart}
                                source={require('../assets/cart.png')}/>
                        </View>
                        <Animated.View style={[styles.containerSearchBar, {marginTop: searchbarTop}]}>
                            <View style={styles.searchBar}>
                                <Image source={require('../assets/searchbar_image.png')}
                                        style={styles.iconSearchBar}/>
                                <TextInput style={styles.inputSearchBar}
                                    placeholder="Bạn tìm gì hôm nay ..."
                                    />
                            </View>
                            <Animated.View style={[styles.searchBarOffset, {width: searchbarWidth}]}/>
                        </Animated.View>
                        {/* <Animated.View style={[styles.banner, {marginTop: bannerTop}]}/> */}
                        
                    </View>
                        <Animated.View style={[styles.banner, {marginTop: bannerTop}]}>
                            <ScrollBanner style={{width: '100%', height: '100%'}} />
                        </Animated.View>                    


                </Animated.View>
            </View>
        );
    }

    _renderNonSense = () => {
        return this.array.map
        (i => (
            <View style={styles.item}>
                <Text style={{fontSize: 18}}>{i}</Text>
            </View>
        ));
    }
}

class LoopXtranslateAnimation extends Component{
    constructor(props){
        super(props)
        this.state = {
            xTranslate: Animated.Value(0)
        }
    }

    componentWillMount(){
        // Animated.loop(
        //     Animated.sequence([
        //       Animated.timing(this.state.xTranslate, {
        //         toValue: 1,
        //         duration: 500,
        //         delay: 1000
        //       }),
        //       Animated.timing(this.state.xTranslate, {
        //         toValue: 0,
        //         duration: 500
        //       })
        //     ]),{ iterations: 4 }
        // ).start()

        Animated.timing(this.state.xTranslate, {
            toValue: -100,
            duration: 1000
        }).start()
    }
    
    render(){
        return(
            <Animated.View
                style={{...this.props.style, transform: [{translateX: this.state.xTranslate}]}}>
                    {this.props.children}
                        {/* để render() các children của Component FadeAnimation */}
            </Animated.View>
        );
    }
}