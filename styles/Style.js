import React, {Component} from 'react'
import {StyleSheet} from 'react-native'
import {GLOBAL} from '../Global/Global'
import { Global } from '@jest/types';

export default Styles = StyleSheet.create({
    //Home style
    container: {
        flex: 1,
        backgroundColor: '#fafafa',
        paddingTop: (Platform.OS == 'ios') ? 20 : 0,
    },
    animatedHeader:
    {
        flex: 1,
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        // justifyContent: 'center',
        alignItems: 'center'
    },
    backgroundImage: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        width: null,
        resizeMode: 'cover',
        borderBottomLeftRadius: GLOBAL.HEADER_MAX_HEIGHT,
        borderBottomRightRadius: GLOBAL.HEADER_MAX_HEIGHT,
        transform:[{scaleX: 2}]
    },
    item:
    {
        backgroundColor: '#E0E0E0',
        marginVertical: 10,
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center'
    },
    scrollContent:{
        paddingTop: GLOBAL.HEADER_MAX_HEIGHT + 20,
        overflow: 'visible'
    },
    contentHeader:{
        width: '90%',
        flex: 1,
        alignItems: 'center',
        // backgroundColor: 'red',
        zIndex: 100,
        // marginBottom: 15
    },
    containerLogo:{
        width: '100%',
        height: 40,
        position: 'absolute',
        top: 30,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    logo:{
        resizeMode: 'contain',
        width: 40,
        height: 40
    },
    searchBar:{
        backgroundColor: '#eeeeee',
        borderRadius: 5,
        flexDirection: 'row',
        alignItems: "center",
        flex: 1,
        height: '100%'
    },
    searchBarOffset:{
        width: GLOBAL.SIZE_CART,
        height: GLOBAL.SIZE_CART
    },
    cart:{
        width: GLOBAL.SIZE_CART,
        height: GLOBAL.SIZE_CART,
        position: 'absolute',
        right: 0,
        resizeMode: 'contain'
    },
    iconSearchBar:{
        height: '50%', 
        width: undefined, 
        aspectRatio: 1, 
        marginLeft: 10
    },
    inputSearchBar:{
        flex:1, 
        marginLeft: 10, 
        fontSize: 16, 
        marginRight: 10
    },
    containerSearchBar:{
        width: '100%',
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        // zIndex: 100
    },
    banner:{
        width: '100%',
        height: 100,
        // borderRadius: 7,
        // backgroundColor: '#64ffda',
        position: 'absolute',
        marginTop: 130,
        // bottom: -25

    }
})